﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Web;
using PatientsDashboard.Models;

namespace PatientsDashboard.ViewModels
{
    public class AssessmentsViewModel
    {
        [DisplayName("Have you lost or gained a lot of weight in the last 1-3 months?")]
        public bool LostWeightVal { get; set; }
        [DisplayName("Frequent or severe headaches")]
        public bool SevereHeadachesVal { get; set; }
        [DisplayName("Fainting spells or blackouts")]
        public bool FaintingSpellsVal { get; set; }
        [DisplayName("Vision problems, eye injuries or disorders")]
        public bool VisionProblemsVal { get; set; }
        [DisplayName("Ringing in the ears or reduced hearing")]
        public bool ReducedHearingVal { get; set; }
        [DisplayName("Persistent cough")]
        public bool PersistentCoughVal { get; set; }
        [DisplayName("Chest pain or chest pressure")]
        public bool ChestPainVal { get; set; }
        [DisplayName("Shortness of breath or wheezing")]
        public bool ShortnessBreathVal { get; set; }
        [DisplayName("Frequent or painful urination")]
        public bool PainfulUrinationVal { get; set; }
        [DisplayName("Blood in your urine")]
        public bool BloodyUrineVal { get; set; }
        [DisplayName("Repeated episodes of back or neck pain")]
        public bool NeckPainVal { get; set; }
        [DisplayName("Muscle, bone, or joint injuries")]
        public bool MuscleInjuriesVal { get; set; }
        [DisplayName("Painful or swollen joints")]
        public bool PainfulJointsVal { get; set; }
        [DisplayName("Skin problems (e.g. eczema, dermatitis)")]
        public bool SkinProblemsVal { get; set; }
        [DisplayName("A sore which does not heal")]
        public bool Sores { get; set; }
        [DisplayName("(Females) Gynecologic symptoms or disorders")]
        public bool GynecologicVal { get; set; }
        [DisplayName("Are you now or ever been on treatment for high blood sugar/ diabetes")]
        public bool DiabetesVal { get; set; }
        [DisplayName("Are you now or ever been on treatment for high blood pressure/ hypertension")]
        public bool HypertensionVal { get; set; }
        [DisplayName("Are you now or ever been on treatment for asthma")]
        public bool Asthma { get; set; }
        [DisplayName("Do you take alcohol?")]
        public bool Alcohol { get; set; }
        [DisplayName("Do you smoke cigarettes or chew tobacco?")]
        public bool Tobacco { get; set; }
        [DisplayName("What is your current occupation?")]
        public string Occupation { get; set; }
        [DisplayName("How long have you worked in your current occupation?")]
        public string JobLength { get; set; }
        [DisplayName("What occupational hazards are you exposed to?")]
        public string Hazards { get; set; }
        [DisplayName("Any other problems")]
        public string Other { get; set; }
        [DisplayName("Write any medications you are taking now")]
        public string Medications { get; set; }
    }
}