﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientsDashboard.Models;

namespace PatientsDashboard.ViewModels
{
    public class VisitViewModel
    {
        public List<PatientNotes_Result> PatientNotes { get; set; }
        public List<PatientDiagnosis_Result> PatientDiagnosis { get; set; }
        public List<PatientSigns_Result> PatientSigns { get; set; }
        public List<PatientSymptoms_Result> PatientSymptoms { get; set; }
        public List<PatientVitalsProc_Result> PatientVitals { get; set; }
        public List<PatientDrugs_Result> PatientTreatment { get; set; }

    }
}