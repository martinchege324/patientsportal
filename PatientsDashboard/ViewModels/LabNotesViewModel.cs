﻿using PatientsDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PatientsDashboard.ViewModels
{
    public class LabNotesViewModel
    {
        public List<PatientNotes_Result> PatientNotes { get; set; }
        public List<PatientLabResults_Result> PatientLabs { get; set; }
    }
}