﻿using System.Web.Optimization;

namespace PatientsDashboard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bower_components/jquery-ui").Include(
                "~/bower_components/jquery-ui/jquery-ui.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/admin-lte/plugins/bootstrap-wysihtml5").Include(
                "~/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"));

            bundles.Add(new ScriptBundle("~/admin-lte/js").Include(
                "~/bower_components/jquery-slimscroll/jquery.slimscroll.js",
                "~/bower_components/fastclick/lib/fastclick.js",
                "~/admin-lte/js/adminlte.js",
                "~/admin-lte/js/pages/dashboard.js",
                "~/admin-lte/js/demo.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bower_components/morris").Include(
                "~/bower_components/raphael/raphael.min.js",
                "~/bower_components/morris.js/morris.min.js"));


            bundles.Add(new ScriptBundle("~/bower_components/jquery-sparkline").Include(
                "~/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"));

            bundles.Add(new ScriptBundle("~/admin-lte/plugins/jvectormap").Include(
                "~/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                "~/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"));

            bundles.Add(new ScriptBundle("~/bower_components/jquery-knob").Include(
                "~/bower_components/jquery-knob/dist/jquery.knob.min.js"));

            bundles.Add(new ScriptBundle("~/bower_components/moment").Include(
                "~/bower_components/moment/min/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bower_components/bootstrap-daterangepicker").Include(
                "~/bower_components/bootstrap-daterangepicker/daterangepicker.js"
            ));

            bundles.Add(new ScriptBundle("~/bower_components/bootstrap-datepicker").Include(
                "~/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"));

            bundles.Add(new ScriptBundle("~/admin-lte/css/bootstrap-wysihtml5").Include(
                "~/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.jss"));

            bundles.Add(new ScriptBundle("~/bower_components/jquery-slimscroll").Include(
                "~/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/bower_components/fastclick").Include(
                "~/bower_components/fastclick/lib/fastclick.js"));

            bundles.Add(new ScriptBundle("~/admin-lte/plugins/iCheck").Include(
                "~/admin-lte/plugins/iCheck/icheck.min.js"));

            bundles.Add(new ScriptBundle("~/admin-lte/js/pages").Include(
                "~/admin-lte/js/pages/dashboard3.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.css",
                "~/Content/Site.css"
            ));
            bundles.Add(new StyleBundle("~/admin-lte/css").Include(
                "~/bower_components/Ionicons/css/ionicons.min.css",
                "~/admin-lte/css/AdminLTE.min.css",
                "~/admin-lte/plugins/iCheck/square/blue.css"));

            bundles.Add(new StyleBundle("~/all-skins/css").Include(
                "~/admin-lte/css/skins/_all-skins.min.css",
                "~/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));

            bundles.Add(new StyleBundle("~/bower_components/styles").Include(
                "~/bower_components/morris.js/morris.css",
                "~/bower_components/jvectormap/jquery-jvectormap.css",
                "~/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                "~/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                "~/bower_components/Ionicons/css/ionicons.min.css"));

            bundles.Add(new StyleBundle("~/admin-lte/css/iCheck").Include(
                "~/admin-lte/plugins/iCheck/square/blue.css"));

            bundles.Add(new StyleBundle("~/bower_components/select2").Include(
                "~/bower_components/select2/dist/css/select2.min.css"));
            BundleTable.EnableOptimizations = false;
        }
    }
}