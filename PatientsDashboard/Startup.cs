﻿using Microsoft.Owin;
using Owin;
using PatientsDashboard;

[assembly: OwinStartupAttribute(typeof(Startup))]

namespace PatientsDashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

        }
    }
}