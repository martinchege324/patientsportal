﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientsDashboard.ViewModels;
using PatientsDashboard.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace PatientsDashboard.Controllers
{
    public class AssessmentsController : Controller
    {
        // GET: Assessments
        public async Task<ActionResult> AssessmentsView()
        {
            using (ProcedureModels entities = new ProcedureModels())
            {
               
                return View();
            }

        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssessmentsPost(AssessmentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (ProcedureModels entities = new ProcedureModels())
                {
                    string user = Session["patientID"].ToString();
                    Guid userID = Guid.Parse(user);
                    entities.AssessmentsTransProc(1, userID, model.LostWeightVal);
                    entities.AssessmentsTransProc(2, userID, model.SevereHeadachesVal);
                    entities.AssessmentsTransProc(3, userID, model.FaintingSpellsVal);
                    entities.AssessmentsTransProc(4, userID, model.VisionProblemsVal);
                    entities.AssessmentsTransProc(5, userID, model.ReducedHearingVal);
                    entities.AssessmentsTransProc(6, userID, model.PersistentCoughVal);
                    entities.AssessmentsTransProc(7, userID, model.ChestPainVal);
                    entities.AssessmentsTransProc(8, userID, model.ShortnessBreathVal);
                    entities.AssessmentsTransProc(9, userID, model.PainfulUrinationVal);
                    entities.AssessmentsTransProc(10, userID, model.BloodyUrineVal);
                    entities.AssessmentsTransProc(11, userID, model.NeckPainVal);
                    entities.AssessmentsTransProc(12, userID, model.MuscleInjuriesVal);
                    entities.AssessmentsTransProc(13, userID, model.PainfulJointsVal);
                    entities.AssessmentsTransProc(14, userID, model.SkinProblemsVal);
                    entities.AssessmentsTransProc(15, userID, model.Sores);
                    entities.AssessmentsTransProc(16, userID, model.GynecologicVal);

                    entities.AssessmentsTransProc(17, userID, model.DiabetesVal);
                    entities.AssessmentsTransProc(18, userID, model.HypertensionVal);
                    entities.AssessmentsTransProc(19, userID, model.Asthma);
                    entities.AssessmentsTransProc(20, userID, model.Alcohol);
                    entities.AssessmentsTransProc(21, userID, model.Tobacco);
                    
                    
                    entities.OccupationsTransProc(1, userID, model.Occupation);
                    entities.OccupationsTransProc(2, userID, model.JobLength);
                    entities.OccupationsTransProc(3, userID, model.Hazards);
                    entities.OccupationsTransProc(4, userID, model.Other);
                    entities.OccupationsTransProc(5, userID, model.Medications);
                    
                    return View("AssessmentsSuccess");
                }
            }
            else 
            {
                return View(model);
            }
        }
           

        
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> getScheme()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {

                return Json(await _entities.patient_payment_account_types.Select(c => new { c.scheme_code, c.patient_payment_account_type_desc }).OrderBy(c => c.scheme_code).ToListAsync(), JsonRequestBehavior.AllowGet);
            }

        }

    }
}
