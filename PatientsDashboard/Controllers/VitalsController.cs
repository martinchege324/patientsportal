﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientsDashboard.ActionFilters;
using PatientsDashboard.Models;
using log4net;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    //[OutputCache(Duration = 480, VaryByParam = "none")]
    [CompressFilter]
    [ETag]
    public class VitalsController : Controller
    {
        ProcedureModels _entities = new ProcedureModels();
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        // GET: Vitals
        public ActionResult VitalsView()
        {
            return View();
        }

        public JsonResult getVisits()
        {
            var visits = _entities.PatientVisits(Session["patientID"].ToString()).OrderByDescending(c => DateTime.Parse(c.cycle_created_time.ToString()).Date).ToList();
            List<PatientVisits_Result> list = new List<PatientVisits_Result>();
            foreach (var item in visits)
            {
                var cycles = new PatientVisits_Result();
                cycles.visitDate = string.Format("{0:dd-MM-yyyy}", item.cycle_created_time);
                cycles.cycle_id = item.cycle_id;
                list.Add(cycles);

            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        //Get Vitals based on the Visit Date Selected

        [HttpPost]
        public ActionResult _Vitals(string cycle)
        {
            
            string user = Session["patientID"].ToString();
            Logger.Info(user);
            Logger.Debug("Accessing patient vitals");
            var vitals = _entities.PatientVitalsProc(cycle).ToList();
            if (vitals.Any())
            {
                return PartialView("_Vitals", vitals);
            }
            else
            {
                return Content("");
            }
            

        }

    }
}
