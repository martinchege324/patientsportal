﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientsDashboard.ActionFilters;
using PatientsDashboard.Models;
using PatientsDashboard.ViewModels;
using log4net;
using System.Text;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    [CompressFilter]
    [ETag]

    public class LabTestController : Controller
    {
        ProcedureModels _entities = new ProcedureModels();
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        // GET: LabTest
        public ActionResult LabTestView()
        {

            return View();
        }
        public ActionResult _labResults(string cycle)
        {

           
        
            string user = Session["patientID"].ToString();
            Logger.Info(user);
            Logger.Debug("Accessing patient labs");
            StringBuilder stringBuilder = new StringBuilder();
            List<PatientLabResults_Result> labResults = _entities.PatientLabResults(cycle).OrderByDescending(c => c.cycle_created_time).OrderBy(c => c.TEST).ToList();
            List<PatientNotes_Result> patientNotes = _entities.PatientNotes(cycle).OrderBy(c => c.notes_date).ToList();
            List<PatientNotes_Result> final = new List<PatientNotes_Result>();
            foreach (var item in patientNotes)
            {
                if ((item.notes_desc.ToLower().Contains("lab")) && (item.notes_desc.ToLower().Contains("results")) || (item.notes_desc.ToLower().Contains("recommendations")))
                {
                    stringBuilder.Append(item.notes_desc);
                    stringBuilder.AppendLine();
                    stringBuilder.AppendLine();
                }
                final.Add(item);

            }
            ViewBag.recommendations = stringBuilder.ToString();

            var view = new LabNotesViewModel
            {

                PatientNotes = final,
                PatientLabs = labResults

            };

            return PartialView("_labResults", view);
        }
    }
}
