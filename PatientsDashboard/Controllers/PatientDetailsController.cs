﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientsDashboard.ActionFilters;
using PatientsDashboard.Models;
using log4net;
using log4net.Repository.Hierarchy;
using System;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    //[OutputCache(Duration = 480, VaryByParam = "none")]
    [CompressFilter]
    [ETag]
    public class PatientDetailsController : Controller
    {
        ProcedureModels _entities = new ProcedureModels();
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);

        // GET: PatientDetails
        public ActionResult PatientDetailsView()
        {
            
                string user = Session["patientID"].ToString();
                Logger.Info(user);
                Logger.Debug("Accessing patient details");
                List<PatientDetails_Result> details = _entities.PatientDetails(Session["patientID"].ToString()).OrderByDescending(c => c.Date).Take(1).ToList();
                return View(details);
            


          

        }

        // GET: PatientDetails/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PatientDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PatientDetails/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            

            return RedirectToAction("Index");
            

        }

        // GET: PatientDetails/Edit/5
        public ActionResult PatientEdit()
        {
            return View();
        }

        // POST: PatientDetails/Edit/5
        [HttpPost]
        public ActionResult PatientEdit(int id, FormCollection collection)
        {
            
                // TODO: Add update logic here

            return RedirectToAction("Index");
            

        }

        // GET: PatientDetails/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PatientDetails/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {

            // TODO: Add delete logic here

            return RedirectToAction("Index");

        }
    }
}
