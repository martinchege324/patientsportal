﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientsDashboard.ActionFilters;
using PatientsDashboard.Models;
using PatientsDashboard.ViewModels;
using log4net;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    //[OutputCache(Duration = 480, VaryByParam = "none")]
    [CompressFilter]
    [ETag]
    public class VisitSummaryController : Controller
    {
        ProcedureModels _entities = new ProcedureModels();
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        // GET: Vitals
        // GET: VisitSummary
        public ActionResult VisitSummaryView()
        {

            return View();

        }
        public ActionResult _VisitSummaries(string cycle)
        {

            string user = Session["patientID"].ToString();
            Logger.Info(user);
            Logger.Debug("Accessing patient visit summary");
            List<PatientVitalsProc_Result> patientVitals = _entities.PatientVitalsProc(cycle).ToList();
            List<PatientSigns_Result> patientSigns = _entities.PatientSigns(cycle).ToList();
            List<PatientSymptoms_Result> patientSymptoms = _entities.PatientSymptoms(cycle).ToList();
            List<PatientNotes_Result> patientNotes = _entities.PatientNotes(cycle).ToList();
            List<PatientDiagnosis_Result> patientDiagnosis = _entities.PatientDiagnosis(cycle).ToList();
            List<PatientDrugs_Result> patientDrugs = _entities.PatientDrugs(cycle).ToList();
            List<PatientDrugs_Result> list = new List<PatientDrugs_Result>();
            foreach (var item in patientDrugs)
            {
                var newItem = new PatientDrugs_Result();
                newItem.center_product_description = item.center_product_description;
                newItem.cycle_id = item.cycle_id;
                newItem.product_code = item.product_code;
                newItem.unit_price = item.unit_price;
                newItem.trans_quantity = item.trans_quantity;
                newItem.total = Convert.ToDecimal(newItem.trans_quantity) * newItem.unit_price;
                newItem.trans_date = item.trans_date;
                newItem.regular_retail_price = item.regular_retail_price;
                newItem.vatvalue = item.vatvalue;
                newItem.savings = item.savings;
                list.Add(newItem);
            }
            VisitViewModel view = new VisitViewModel
            {

                PatientVitals = patientVitals,
                PatientNotes = patientNotes,
                PatientDiagnosis = patientDiagnosis,
                PatientSigns = patientSigns,
                PatientSymptoms = patientSymptoms,
                PatientTreatment = list
            };
            return PartialView("_VisitSummaries", view);
        }
            


    }
}