﻿using System.Web.Mvc;
using PatientsDashboard.Models;
using PatientsDashboard.ActionFilters;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    [OutputCache(Duration = 480, VaryByParam = "none")]
    [CompressFilter]
    [ETag]
    public class HomeController : Controller
    {
        ProcedureModels entities = new ProcedureModels();
        //Home Action Method
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}