﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientsDashboard.Models;
using System;
using PatientsDashboard.ActionFilters;
using log4net;

namespace PatientsDashboard.Controllers
{
    [Authorize]
    //[OutputCache(Duration = 480, VaryByParam = "none")]
    [CompressFilter]
    [ETag]
    public class DrugsController : Controller
    {
        ProcedureModels _entities = new ProcedureModels();
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        // GET: Most recent drugs purchased by the user and display a drop down of the visits, allowing the user to select any visit to display the results
        public ActionResult PatientDrugsView()
        {
            return View();
        }

        public ActionResult _DrugsView(string cycle)
        {
            string user = Session["patientID"].ToString();
            Logger.Info(user);
            Logger.Debug("Accessing patient drugs");
            
          
            //Call the drugs procedure
            List<PatientDrugs_Result> drugList = _entities.PatientDrugs(cycle).OrderByDescending(c => c.trans_date).ToList();
            List<PatientDrugs_Result> list = new List<PatientDrugs_Result>();
            foreach (var item in drugList)
            {
                var newItem = new PatientDrugs_Result();
                newItem.center_product_description = item.center_product_description;
                newItem.cycle_id = item.cycle_id;
                newItem.product_code = item.product_code;
                newItem.unit_price = item.unit_price;
                newItem.trans_quantity = item.trans_quantity;
                newItem.total = Convert.ToDecimal(newItem.trans_quantity) * newItem.unit_price;
                newItem.trans_date = item.trans_date;
                newItem.regular_retail_price = item.regular_retail_price;
                newItem.vatvalue = item.vatvalue;
                newItem.savings = item.savings;
                list.Add(newItem);
            }

            //Summarize totals, savings and %Savings

            var summary = list.AsEnumerable();

            if (summary.Sum(c => c.total).HasValue)
            {
                ViewBag.Total = $"{summary.Sum(c => c.total):#,###0}";
            }
            else
            {
                ViewBag.Total = 0;
            }


            var savings = summary.Sum(c => c.savings);
            if (!savings.HasValue)
            {
                savings = 0;
            }

            var regularTotal = summary.Sum(c => c.regular_retail_price);
            if (savings != 0 && regularTotal != 0)
            {
                var percentageSavings = (savings / regularTotal) * 100;
                ViewBag.PercentageSavings = $"{percentageSavings:#,###0}";
            }
            else
            {
                var percentageSavings = 0;
                ViewBag.PercentageSavings = percentageSavings;
            }


            ViewBag.Savings = $"{savings:#,###0}";
            if (drugList.Any())
            {
                return PartialView("_DrugsView", list);
            }
            else
            {
                return Content("");
            }



            

        }
    }
}
