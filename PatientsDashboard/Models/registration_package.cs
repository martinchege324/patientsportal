//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PatientsDashboard.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class registration_package
    {
        public System.Guid package_id { get; set; }
        public string package_description { get; set; }
        public Nullable<int> visit_count { get; set; }
        public string package_code { get; set; }
        public Nullable<int> center_id { get; set; }
        public Nullable<int> family_count { get; set; }
        public Nullable<int> facility_id { get; set; }
    }
}
