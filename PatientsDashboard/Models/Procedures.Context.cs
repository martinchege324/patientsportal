﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PatientsDashboard.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ProcedureModels : DbContext
    {
        public ProcedureModels()
            : base("name=ProcedureModels")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<registration_package> registration_package { get; set; }
        public virtual DbSet<patient_asssessments> patient_asssessments { get; set; }
        public virtual DbSet<patient_asssessments_occupations> patient_asssessments_occupations { get; set; }
        public virtual DbSet<patient_asssessments_occupations_trans> patient_asssessments_occupations_trans { get; set; }
        public virtual DbSet<patient_asssessments_trans> patient_asssessments_trans { get; set; }
        public virtual DbSet<patient_login> patient_login { get; set; }
        public virtual DbSet<patient_payment_account_types> patient_payment_account_types { get; set; }
        public virtual DbSet<patient_portal_registration> patient_portal_registration { get; set; }
    
        public virtual ObjectResult<PatientDetails_Result> PatientDetails(string patientID)
        {
            var patientIDParameter = patientID != null ?
                new ObjectParameter("patientID", patientID) :
                new ObjectParameter("patientID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientDetails_Result>("PatientDetails", patientIDParameter);
        }
    
        public virtual ObjectResult<PatientLogin_Result> PatientLogin(string phone, string password)
        {
            var phoneParameter = phone != null ?
                new ObjectParameter("phone", phone) :
                new ObjectParameter("phone", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientLogin_Result>("PatientLogin", phoneParameter, passwordParameter);
        }
    
        public virtual int UpdatePatientDetails(string phone, string patient_first_name, string patient_middle_name, string patient_last_name, string gender, Nullable<System.DateTime> dob, string patient_tel, string patient_address, string patient_fathers_name, string patient_mothers_name, string email)
        {
            var phoneParameter = phone != null ?
                new ObjectParameter("phone", phone) :
                new ObjectParameter("phone", typeof(string));
    
            var patient_first_nameParameter = patient_first_name != null ?
                new ObjectParameter("patient_first_name", patient_first_name) :
                new ObjectParameter("patient_first_name", typeof(string));
    
            var patient_middle_nameParameter = patient_middle_name != null ?
                new ObjectParameter("patient_middle_name", patient_middle_name) :
                new ObjectParameter("patient_middle_name", typeof(string));
    
            var patient_last_nameParameter = patient_last_name != null ?
                new ObjectParameter("patient_last_name", patient_last_name) :
                new ObjectParameter("patient_last_name", typeof(string));
    
            var genderParameter = gender != null ?
                new ObjectParameter("gender", gender) :
                new ObjectParameter("gender", typeof(string));
    
            var dobParameter = dob.HasValue ?
                new ObjectParameter("dob", dob) :
                new ObjectParameter("dob", typeof(System.DateTime));
    
            var patient_telParameter = patient_tel != null ?
                new ObjectParameter("patient_tel", patient_tel) :
                new ObjectParameter("patient_tel", typeof(string));
    
            var patient_addressParameter = patient_address != null ?
                new ObjectParameter("patient_address", patient_address) :
                new ObjectParameter("patient_address", typeof(string));
    
            var patient_fathers_nameParameter = patient_fathers_name != null ?
                new ObjectParameter("patient_fathers_name", patient_fathers_name) :
                new ObjectParameter("patient_fathers_name", typeof(string));
    
            var patient_mothers_nameParameter = patient_mothers_name != null ?
                new ObjectParameter("patient_mothers_name", patient_mothers_name) :
                new ObjectParameter("patient_mothers_name", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("email", email) :
                new ObjectParameter("email", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdatePatientDetails", phoneParameter, patient_first_nameParameter, patient_middle_nameParameter, patient_last_nameParameter, genderParameter, dobParameter, patient_telParameter, patient_addressParameter, patient_fathers_nameParameter, patient_mothers_nameParameter, emailParameter);
        }
    
        public virtual ObjectResult<PatientVisits_Result> PatientVisits(string patientID)
        {
            var patientIDParameter = patientID != null ?
                new ObjectParameter("patientID", patientID) :
                new ObjectParameter("patientID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientVisits_Result>("PatientVisits", patientIDParameter);
        }
    
        public virtual ObjectResult<PatientDrugs_Result> PatientDrugs(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientDrugs_Result>("PatientDrugs", cycleIDParameter);
        }
    
        public virtual ObjectResult<PatientLabResults_Result> PatientLabResults(string cycle_id)
        {
            var cycle_idParameter = cycle_id != null ?
                new ObjectParameter("cycle_id", cycle_id) :
                new ObjectParameter("cycle_id", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientLabResults_Result>("PatientLabResults", cycle_idParameter);
        }
    
        public virtual ObjectResult<PatientVitalsProc_Result> PatientVitalsProc(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientVitalsProc_Result>("PatientVitalsProc", cycleIDParameter);
        }
    
        public virtual ObjectResult<PatientAppointments_Result> PatientAppointments(string patientID)
        {
            var patientIDParameter = patientID != null ?
                new ObjectParameter("patientID", patientID) :
                new ObjectParameter("patientID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientAppointments_Result>("PatientAppointments", patientIDParameter);
        }
    
        public virtual ObjectResult<PatientDiagnosis_Result> PatientDiagnosis(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientDiagnosis_Result>("PatientDiagnosis", cycleIDParameter);
        }
    
        public virtual ObjectResult<PatientNotes_Result> PatientNotes(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientNotes_Result>("PatientNotes", cycleIDParameter);
        }
    
        public virtual ObjectResult<PatientSigns_Result> PatientSigns(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientSigns_Result>("PatientSigns", cycleIDParameter);
        }
    
        public virtual ObjectResult<PatientSymptoms_Result> PatientSymptoms(string cycleID)
        {
            var cycleIDParameter = cycleID != null ?
                new ObjectParameter("cycleID", cycleID) :
                new ObjectParameter("cycleID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PatientSymptoms_Result>("PatientSymptoms", cycleIDParameter);
        }
    
        public virtual int patientDashboardLogs(string thread, string level, string logger, string message, string exception)
        {
            var threadParameter = thread != null ?
                new ObjectParameter("Thread", thread) :
                new ObjectParameter("Thread", typeof(string));
    
            var levelParameter = level != null ?
                new ObjectParameter("Level", level) :
                new ObjectParameter("Level", typeof(string));
    
            var loggerParameter = logger != null ?
                new ObjectParameter("Logger", logger) :
                new ObjectParameter("Logger", typeof(string));
    
            var messageParameter = message != null ?
                new ObjectParameter("Message", message) :
                new ObjectParameter("Message", typeof(string));
    
            var exceptionParameter = exception != null ?
                new ObjectParameter("Exception", exception) :
                new ObjectParameter("Exception", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("patientDashboardLogs", threadParameter, levelParameter, loggerParameter, messageParameter, exceptionParameter);
        }
    
        public virtual int SignUpPatientProc(string surname, string otherNames, string email, string phone, string iDNumber, string company, string patientFacilityNumber, Nullable<System.DateTime> dOB)
        {
            var surnameParameter = surname != null ?
                new ObjectParameter("Surname", surname) :
                new ObjectParameter("Surname", typeof(string));
    
            var otherNamesParameter = otherNames != null ?
                new ObjectParameter("OtherNames", otherNames) :
                new ObjectParameter("OtherNames", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var phoneParameter = phone != null ?
                new ObjectParameter("Phone", phone) :
                new ObjectParameter("Phone", typeof(string));
    
            var iDNumberParameter = iDNumber != null ?
                new ObjectParameter("IDNumber", iDNumber) :
                new ObjectParameter("IDNumber", typeof(string));
    
            var companyParameter = company != null ?
                new ObjectParameter("Company", company) :
                new ObjectParameter("Company", typeof(string));
    
            var patientFacilityNumberParameter = patientFacilityNumber != null ?
                new ObjectParameter("PatientFacilityNumber", patientFacilityNumber) :
                new ObjectParameter("PatientFacilityNumber", typeof(string));
    
            var dOBParameter = dOB.HasValue ?
                new ObjectParameter("DOB", dOB) :
                new ObjectParameter("DOB", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SignUpPatientProc", surnameParameter, otherNamesParameter, emailParameter, phoneParameter, iDNumberParameter, companyParameter, patientFacilityNumberParameter, dOBParameter);
        }
    
        public virtual int UpdateSGXProc(Nullable<int> visitCount)
        {
            var visitCountParameter = visitCount.HasValue ?
                new ObjectParameter("VisitCount", visitCount) :
                new ObjectParameter("VisitCount", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateSGXProc", visitCountParameter);
        }
    
        public virtual int AssessmentsTransProc(Nullable<int> assessmentID, Nullable<System.Guid> patientID, Nullable<bool> assesssmentValue)
        {
            var assessmentIDParameter = assessmentID.HasValue ?
                new ObjectParameter("AssessmentID", assessmentID) :
                new ObjectParameter("AssessmentID", typeof(int));
    
            var patientIDParameter = patientID.HasValue ?
                new ObjectParameter("PatientID", patientID) :
                new ObjectParameter("PatientID", typeof(System.Guid));
    
            var assesssmentValueParameter = assesssmentValue.HasValue ?
                new ObjectParameter("AssesssmentValue", assesssmentValue) :
                new ObjectParameter("AssesssmentValue", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AssessmentsTransProc", assessmentIDParameter, patientIDParameter, assesssmentValueParameter);
        }
    
        public virtual int OccupationsTransProc(Nullable<int> assessmentID, Nullable<System.Guid> patientID, string assesssmentValue)
        {
            var assessmentIDParameter = assessmentID.HasValue ?
                new ObjectParameter("AssessmentID", assessmentID) :
                new ObjectParameter("AssessmentID", typeof(int));
    
            var patientIDParameter = patientID.HasValue ?
                new ObjectParameter("PatientID", patientID) :
                new ObjectParameter("PatientID", typeof(System.Guid));
    
            var assesssmentValueParameter = assesssmentValue != null ?
                new ObjectParameter("AssesssmentValue", assesssmentValue) :
                new ObjectParameter("AssesssmentValue", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("OccupationsTransProc", assessmentIDParameter, patientIDParameter, assesssmentValueParameter);
        }
    }
}
