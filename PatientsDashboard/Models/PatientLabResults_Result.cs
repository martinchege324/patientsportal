//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PatientsDashboard.Models
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class PatientLabResults_Result
    {
        public string INVOICE { get; set; }
        public string OPNO { get; set; }
        public string NAMES { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [DisplayName("Date")]
        public System.DateTime cycle_created_time { get; set; }
        public string GENDER { get; set; }
        public Nullable<int> AGE { get; set; }
        public string LABNO { get; set; }
        public string SAMPLE { get; set; }
        public int sub_lab_test_id { get; set; }
        [DisplayName("Test")]
        public string TEST { get; set; }
        [DisplayName("Result")]
        public string RESULT { get; set; }
        [DisplayName("Value")]
        public string VALUE { get; set; }
        [DisplayName("Ref")]
        public string REF { get; set; }
        [DisplayName("Test")]
        public string LOWREF { get; set; }
        public string HIGHREF { get; set; }
        [DisplayName("Status")]
        public string STATUS { get; set; }
    }
}
